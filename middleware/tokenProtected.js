const Token = require('../helpers/token');

module.exports = (req, res, next) => {
    const token = req.headers.authorization;
  
    if (!token) return res.status(403).send('The security token is not present.')
  
    if (Token.getToken(token.split(' ')[1]) === null) {
        return res.status(403).send('The security token is invalid, please try again.');
    }
  
    return next();
  }