const db = require('../db');
const Schema = db.Schema;

const apiLogsSchema = new Schema({
    userIP: {
        type: String,
        required: true
    },
    requestDateTime: {
        type: Date,
        required: true
    },
    searchTerm: {
        type: String,
        default: null
    },
    itemCount: {
        type: Number,
        required: true
    }
});

module.exports = db.model('ApiLogs', apiLogsSchema);