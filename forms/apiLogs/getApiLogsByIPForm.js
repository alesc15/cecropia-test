const ApiLog = require('../../data/models/apiLogs');
const BaseForm = require('../baseForm');
const Validator = require('../../helpers/validator');

class GetApiLogsForm extends BaseForm {
    constructor(form) {
        super();
        this.userIP = form;
    }

    validateFields(messages) {
        Validator.validate(this.userIP, [{ type:'MAX_LENGTH', params: { maxLength: 50 }}], messages);
    }

    async submitAction() {
        const data = await ApiLog.find({ userIP: this.userIP.value }).then();
        return data;
    }
}

module.exports = GetApiLogsForm;