const mongoose = require('mongoose');

const connectionString = process.env.MONGO_URI || 'mongodb://localhost/cecropia-test';
mongoose.connect(connectionString);

// CONNECTION EVENTS
// When successfully connected
mongoose.connection.on('connected', function () {
  console.log('DB Connected');
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  console.error('DB Error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('DB disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('DB disconnected through app termination');
    process.exit(0);
  });
});

module.exports = mongoose;