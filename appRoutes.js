const movies = require('./routes/movies');
const admin = require('./routes/admin');

module.exports = {
    movies,
    admin
};