const ApiLog = require('../../data/models/apiLogs');
const BaseForm = require('../baseForm');

class GetApiLogsForm extends BaseForm {
    constructor() {
        super();
    }

    async submitAction() {
        const data = await ApiLog.find().then();
        
        return data;
    }
}

module.exports = GetApiLogsForm;