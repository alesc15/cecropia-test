const ApiLog = require('../../data/models/apiLogs');
const BaseForm = require('../baseForm');
const Validator = require('../../helpers/validator');

class NewApiLogForm extends BaseForm {
    constructor(form) {
        super();
        this.userIP = form.userIP;
        this.requestDateTime = form.requestDateTime;
        this.searchTerm = form.searchTerm;
        this.itemCount = form.itemCount;
    }

    validateFields(messages) {
        Validator.validate(this.userIP, [{ type:'MAX_LENGTH', params: { maxLength: 50 }}], messages);
        Validator.validate(this.requestDateTime, [{ type:'DATE'}], messages);
        Validator.validate(this.itemCount, [{ type:'NUMBER'}], messages);
    }

    async submitAction() {
        const data = await ApiLog.create({ userIP: this.userIP.value, requestDateTime: this.requestDateTime.value, searchTerm: this.searchTerm.value, itemCount: this.itemCount.value }).then();
        return data;
    }
}

module.exports = NewApiLogForm;