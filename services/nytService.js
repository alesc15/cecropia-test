const axios = require('axios');
const secretKey = 'da4236a4c48a4cf9a0bb3b0d6bede4dc';
const baseUrl = `https://api.nytimes.com/svc/movies/v2/reviews/search.json?api_key=${ secretKey }`;

class NYTService {
    static async getRecentReviews() {
        let reviews = await axios.get(baseUrl).then();
        return reviews.data;
        
    }

    static async getMovieReview(name) {
        let reviews = await axios.get(`${ baseUrl }&query='${ name }'`).then();
        return reviews.data;
    }

}

NYTService.getRecentReviews();

module.exports = NYTService;