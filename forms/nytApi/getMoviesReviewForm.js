const NYTService = require('../../services/nytService');
const BaseForm = require('../baseForm');

class GetMoviesReviewForm extends BaseForm {
    constructor() {
        super();
    }

    async submitAction() {
        const movieReviews = await NYTService.getRecentReviews();
        let data = [];
        

        movieReviews.results.map(movieReview => {
            data.push({
                title: movieReview.display_title,
                thumbnail: movieReview.multimedia.src,
                openingDate: movieReview.opening_date
            });
        });

        return data;
    }
}

module.exports = GetMoviesReviewForm;