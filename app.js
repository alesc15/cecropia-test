const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');

//Middlewares
const TokenProtected = require('./middleware/tokenProtected');
const ApiLogsHandler = require('./middleware/apiLogs');
const OnlyAdmins = require('./middleware/onlyAdmins');
const RequestDateTime = require('./middleware/requestDateTime');

//Routes
const appRoutes = require('./appRoutes');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(RequestDateTime);

app.use('/api/admin', TokenProtected, OnlyAdmins, appRoutes.admin);
app.use('/api/movies', TokenProtected, appRoutes.movies, ApiLogsHandler);

app.use(function(req, res, next) {
  res.status(404).send('Not Found');
});

app.use(function(err, req, res, next) {
  if (process.env.NODE_ENV === 'development') console.error(err.message);
  
  res.send('An unknow error happened. Please contact support and try again later.')
});

module.exports = app;
