const NYTService = require('../../services/nytService');
const BaseForm = require('../baseForm');
const Validator = require('../../helpers/validator');

class GetMovieReviewForm extends BaseForm {
    constructor(form) {
        super();
        this.movieName = form;
    }

    validateFields(messages) {
        Validator.validate(this.movieName, [{ type: 'MAX_LENGTH', params: { maxLength: 50 }}], messages);
    }

    async submitAction() {
        const movieReviews = await NYTService.getMovieReview(this.movieName.value.replace('_', ' '));
        let data = [];
        
        movieReviews.results.map(movieReview => {
            data.push({
                title: movieReview.display_title,
                thumbnail: movieReview.multimedia ? movieReview.multimedia.src : null,
                openingDate: movieReview.opening_date,
                headline: movieReview.headline,
                publicationDate: movieReview.publication_date,
                author: movieReview.byline,
                summary: movieReview.summary_short,
                fullReviewLink: movieReview.link.url
            });
        });

        return data;
    }
}

module.exports = GetMovieReviewForm;