const express = require('express');
const router = express.Router();
const GetMovieReview = require('../forms/nytApi/getMovieReviewForm');
const GetMoviesReview = require('../forms/nytApi/getMoviesReviewForm');

router.get('/', function(req, res, next) {
    new GetMoviesReview().submit().then(data => {
        if (!data.pass) return res.status(500).send(data);
        
        res.locals.data = data;
        next();
    });
});

router.get('/search/:query', function(req, res, next) {
    new GetMovieReview({ label: 'Search Query', value: req.params.query }).submit().then(data => {
        if (!data.pass) return res.status(500).send(data);

        res.locals.data = data;
        next();
    });
});

module.exports = router;