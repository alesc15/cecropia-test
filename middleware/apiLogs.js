const NewApiLogForm = require('../forms/apiLogs/newApiLogForm');

module.exports = async (req, res, next) => {
    if (!res.locals.data) return next();

    try {
        const form = {
            userIP: {
                label: "User IP",
                value: req.connection.remoteAddress
            },
            requestDateTime: {
                label: "Request DateTime",
                value: req.dateTime
            },
            searchTerm: {
                label: "Search Term",
                value: req.params.query || null
            },
            itemCount: {
                label: "Item Count",
                value: res.locals.data.response.length
            }
        }
    
        new NewApiLogForm(form).submit().then(data => {
            if (!data.pass) return res.status(500).send(data);
    
            res.send(res.locals.data);
        });
    } catch (e) {
        res.status(500).send(e.message);
    }
}