const Token = require('../helpers/token');

module.exports = (req, res, next) => {
    try {
        const userInfo = Token.getToken(req.headers.authorization.split(' ')[1]);

        if (userInfo.admin) {
            return next();
        } else {
            return res.status(403).send('You are not authorized to view this page.');
        }
    } catch(e) {
        return res.status(500).send(e.message);
    }
}