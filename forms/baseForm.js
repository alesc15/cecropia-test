let messages;
let pass;

class BaseForm {
    constructor(){
        if (new.target === BaseForm) {
            throw new TypeError('Cannot construct BaseForm directly. You need to extend this class');
        }

        pass = false;
        messages = [];
    }

    async submit() {
        try {
            this.validateFields(messages);
        
            if (messages.length === 0) {
                if (await this.validate(messages)) {
                    const response = await this.submitAction();
                    pass = true;

                    return {
                        pass,
                        response
                    };
                }
            }

        } catch (err) {
           console.error(err.message);
           messages.push('An unknown error has ocurred, please contact support and try again later.')
        }

        return {
            pass,
            response: messages
        };
    }

    validateFields() {}

    async validate() { return true; }

    async submitAction() {}
}

module.exports = BaseForm;