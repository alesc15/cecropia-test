module.exports = (req, res, next) => {
    req.dateTime = Date.now();
    return next();
}