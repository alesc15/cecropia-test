const moment = require('moment');

function validateAlpha(field, messages) {
    const alphaPattern = /^[A-z' ]+$/;

    if (!alphaPattern.test(field.value)) {
        messages.push(field.label + ' has to contain letters only');
    }
}

function validateAlphaNumeric(field, messages) {
    const alphaNumericPattern = /^[A-z0-9_ ]+$/;

    if (!alphaNumericPattern.test(field.value)) {
        messages.push(field.label + ' has to contain alphanumeric characters only');
    }
}

function validateDate(field, messages) {
    if (!moment(field.value).isValid()) {
        messages.push(field.label + ' is not a valid date');
    }
}

function validateEmail(field, messages) {
    const emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (!emailPattern.test(field.value.toLowerCase())) {
        messages.push(field.label + ' is not a valid email address');
    }
}

function validateMaxLength(field, params, messages) {
    if (field.value.length > params.maxLength) {
        messages.push(field.label + ' needs a maximum of ' + params.maxLength + ' characters');
    }
}

function validateMinLength(field, params, messages) {
    if (field.value.length < params.minLength) {
        messages.push(field.label + ' needs a minimum of ' + params.minLength + ' characters');
    }
}

function validateMaxNumber(field, params, messages) {
    if (field.value > params.maxNumber) {
        messages.push(field.label + ' can\'t be higher than ' + params.maxNumber);
    }
}

function validateMinNumber(field, params, messages) {
    if (field.value < params.minNumber) {
        messages.push(field.label + ' can\'t be lower than ' + params.minNumber);
    }
}

function validateNumber(field, messages) {
    if (isNaN(field.value)) {
        messages.push(field.label + ' is not a valid number');
    }
}

function validateRequired(field, messages) {
    if (typeof field.value === 'string' && field.value.length == 0) {
        messages.push(field.label + ' is required');
    } else if (typeof field.value === 'object' && Object.keys(field.value).length == 0) {
        messages.push(field.label + ' is required');
    } else if (field.value == null) {
        messages.push(field.label + ' is required');
    }
}

class Validator {
    static validate(field, validations, messages) {
        console.log(validations);
        
        validations.map(validation => {
            switch (validation.type) {
                case 'ALPHA':
                    validateAlpha(field, messages);
                    break;
                case 'ALPHA_NUMERIC':
                    validateAlphaNumeric(field, messages);
                    break;
                case 'DATE':
                    validateDate(field, messages);
                    break;
                case 'EMAIL':
                    validateEmail(field, messages);
                    break;
                case 'MAX_LENGTH':
                    validateMaxLength(field, validation.params, messages);
                    break;
                case 'MIN_LENGTH':
                    validateMinLength(field, validation.params, messages);
                    break;
                case 'MAX_NUMBER':
                    validateMaxNumber(field, validation.params, messages);
                    break;
                case 'MIN_NUMBER':
                    validateMinNumber(field, validation.params, messages);
                    break;
                case 'NUMBER':
                    validateNumber(field, messages)
                case 'REQUIRED':
                    validateRequired(field, messages);
                    break;
                default:
                    throw new Exception('Validation function does not exist');
            }
        });
    }
}

module.exports = Validator;