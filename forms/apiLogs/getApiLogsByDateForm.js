const ApiLog = require('../../data/models/apiLogs');
const BaseForm = require('../baseForm');
const Validator = require('../../helpers/validator');

class GetApiLogsForm extends BaseForm {
    constructor(form) {
        super();
        form.value = new Date(form.value);
        this.requestDateTime = form;
    }

    validateFields(messages) {
        Validator.validate(this.requestDateTime, [{ type:'DATE' }], messages);
    }

    async submitAction() {
        const data = await ApiLog.find({ requestDateTime: this.requestDateTime.value }).then();
        return data;
    }
}

module.exports = GetApiLogsForm;