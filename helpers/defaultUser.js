const User = require('../data/models/users');
const SHA256 = require('crypto-js/sha256');

module.exports = async () => {
    let userCount = await User.count({ email: 'hello@admin.com'}).then();

    if (userCount === 0) {
        await User.create({ email: 'hello@admin.com', password: SHA256('12345').toString(), name: 'Default Admin', admin: true }).then();
    }

    userCount = await User.count({ email: 'hello@user.com'}).then();

    if (userCount === 0) {
        await User.create({ email: 'hello@user.com', password: SHA256('12345').toString(), name: 'Default User' }).then();
    }
}