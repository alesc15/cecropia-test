const jwt = require('jwt-simple');

const userSecret = process.env.TOKEN_USER_SECRET || 'dev';
const algorithm = 'HS256';

class Token {
    static setToken(id, email, admin = false) {
        let payload = {
            id,
            email,
            admin
        }

        return jwt.encode(payload, userSecret, algorithm);
    }

    static getToken(token) {
        try {
            return jwt.decode(token, userSecret, true, algorithm);
        } catch (ex) {
            console.error(ex.message);
            return null;
        }
    }
}

module.exports = Token;