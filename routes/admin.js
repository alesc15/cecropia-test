const express = require('express');
const router = express.Router();
const GetApiLogsForm = require('../forms/apiLogs/getApiLogsForm');
const GetApiLogsByIPForm = require('../forms/apiLogs/getApiLogsByIPForm');
const GetApiLogsByDateForm = require('../forms/apiLogs/getApiLogsByDateForm');

router.get('/', function(req, res) {
    new GetApiLogsForm().submit().then(data => {
        if (!data.pass) res.status(500);

        res.send(data);
    });
});

router.get('/date/:date', function(req, res, next) {
    new GetApiLogsByDateForm({ label: 'Date', value: req.params.date }).submit().then(data => {
        if (!data.pass) res.status(500);

        res.send(data);
    });
});

router.get('/ip/:ip', function(req, res, next) {
    new GetApiLogsByIPForm({ label: 'User IP', value: req.params.ip }).submit().then(data => {
        if (!data.pass) res.status(500);

        res.send(data);
    });
});

module.exports = router;